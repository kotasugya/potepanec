require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxon) { create(:taxon, name: "Example", parent: taxonomy.root) }
  let(:taxonomy) { create(:taxonomy) }
  let!(:sample_product) { create(:product, name: "Big_bag", taxons: [taxon]) }
  let!(:image) { build(:image) }

  background do
    sample_product.images << image
    visit potepan_category_path(taxon.id)
  end

  context "サイドバーのカテゴリー(taxon)をクリックした時" do
    it "正しいカテゴリーページへ遷移する" do
      click_link "Example"
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  context "商品名をクリックした時" do
    it "プロダクト詳細ページへ遷移する・カテゴリ一覧ページへ戻る" do
      click_link sample_product.name
      expect(current_path).to eq potepan_product_path(sample_product.id)
      click_link "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(sample_product.taxons.first.id)
    end
  end

  it "正しい親カテゴリー(taxonomy)と子カテゴリー(taxon)を表示する" do
    within(:css, '.navbar-side-collapse') do
      expect(page).to have_content 'Brand'
      expect(page).to have_content "Example (#{taxon.all_products.count})"
    end
  end
end
