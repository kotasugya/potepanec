require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /potepan/categories/show" do
    let(:taxon) { create(:taxon) }
    let(:taxonomy) { create(:taxonomy) }
    let(:product) { create(:product) }

    before do
      get potepan_category_path(taxon.id)
    end

    it '200レスポンスを返す' do
      expect(response).to have_http_status(200)
    end

    it '分類群(taxon)を返す' do
      expect(response.body).to include taxon.name
    end

    it '大きな分類(taxonomies)を返す' do
      expect(response.body).to include taxonomy.name
    end

    it 'taxonに紐付けられたproductsを返す' do
      taxon.products.each do |product|
        expect(response.body).to include product.name
        expect(response.body).to include product.display_price
        expect(response.body).to include product.images.attachment.call(:large)
      end
    end
  end
end
