require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET /potepan/products/show" do
    let!(:taxon) { create(:taxon) }
    let!(:other_taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it '200レスポンスを返す' do
      expect(response).to have_http_status(200)
    end

    it '商品名を返す' do
      expect(response.body).to include product.name
    end

    it '商品金額を返す' do
      expect(response.body).to include product.display_price.to_s
    end

    it '商品説明を返す' do
      expect(response.body).to include product.description
    end

    it '商品画像を返す' do
      product.images.each do |image|
        expect(response.body).to include image.attachment.call(:large)
        expect(response.body).to include image.attachment.call(:small)
      end
    end

    it "@related_productsは、関連商品商品を４つもつ" do
      expect(controller.instance_variable_get("@related_products").size).to eq 4
    end
  end
end
