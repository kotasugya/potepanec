require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title(page_title)" do
    subject { full_title(page_title) }

    context 'ページタイトルがnilだった時' do
      let(:page_title) { nil }

      it { expect(subject).to eq("Potepanec") }
    end

    context 'ページタイトルがemptyだった時' do
      let(:page_title) { "" }

      it { expect(subject).to eq("Potepanec") }
    end

    context 'ページタイトルがexampleだった時' do
      let(:page_title) { "example" }

      it { expect(subject).to eq("example | Potepanec") }
    end
  end
end
